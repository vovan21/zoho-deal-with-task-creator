<!DOCTYPE html>
<html>
<head>
	<title>Create deal</title>
	<script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
</head>
<body>
	<p id='msg'></p>
	<form method="POST" action="create">
		@csrf
		<div class="it">
			<b>Create deal</b><br>
			<label>*Deal Name</label>
			<input name="deal_name" ><br>
			<label>Stage</label>
			<input name="stage" ><br>
			<label>Amount</label>
			<input name="amount"><br>
			<label>Lead Source</label>
			<input name="lead_source"><br>
			<label>Probability</label>
			<input name="probability"><br>
			<input type="submit" value="Create deal with task">
		</div>
		<div class="it">
			<b>Associated task</b><br>
			<label>*Subject</label>
			<input name="subject" ><br>
			<label>Status</label>
			<input name="status"><br>
			<label>Priority</label>
			<input name="priority"><br>
			<label>Description</label>
			<input name="description"><br>
		</div>
	</form>
	<style>
		.it{
			display: inline-table;
		}
	</style>
</body>
</html>