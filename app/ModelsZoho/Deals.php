<?php

namespace App\ModelsZoho;

use ZohoCrmSDK\ModelsZoho\PotentialZohoModel as Deal;
use ZohoCrmSDK\ModelsZoho\TaskZohoModel as Task;


class Deals extends Deal
{
	private static $msg;
    private static $dealId;

    public static function createWithTask($req){

        $dataD = $req->except(['subject', 'status', 'priority', '_token']);
        $dataT = $req->only(['subject', 'status', 'priority']);

        if(self::createDeal($dataD)){
            if(self::createRelatedTask($dataT)){
                return 'Success';
            }else{
                return self::$msg;
            }
        }else{
            return self::$msg;
        }

    }
    private static function createDeal($data){
        $deal = parent::new($data);
        $deal->saveToZoho();
        //dump($deal);
        if(is_numeric($deal->id)){
            self::$dealId = $deal->id;
            return true;
        }else
            self::$msg = 'Deal error: '.$deal->id;
    }
    private static function createRelatedTask($data){
        $data['se_module'] = 'Deals';
        $data['what_id'] = self::$dealId;
        $task = Task::new($data);
        $task->saveToZoho();
        //dump($task);
        if(is_numeric($task->id))
            return true;
        else
            self::$msg = 'Task error: '.$task->id;
    }
}