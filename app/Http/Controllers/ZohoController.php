<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelsZoho\Deals;

class ZohoController extends Controller
{
    function getDealForm(){
        return view('deal');
    }
    function create(Request $request){
        return json_encode(['msg' => Deals::createWithTask($request)]);
    }
}
