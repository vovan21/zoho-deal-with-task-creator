<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
\Debugbar::disable();


Route::redirect('/', 'deal');

Route::get('deal', 'App\Http\Controllers\ZohoController@getDealForm');
Route::post('create', 'App\Http\Controllers\ZohoController@create');